# from django.contrib import admin
# from django.urls import path, include
# from django.conf import settings
# from users import view

# from . import views

# urlpatterns = [
    # path('admin/', admin.site.urls),
#     path('', views.homepage.as_view(), name='home'),
# ]




from django.contrib import admin
from django.urls import path, include
# from .master import views
from users import views
from django.contrib.auth import views as auth_view
from django.conf.urls import url
from .views import *

urlpatterns = [
    # path('admin/', admin.site.urls),
    # path('', include('master.urls')),
    # path('users/', include('users.urls')),
    path('', views.homepage.as_view(), name='home'),
    path('login/', views.login_view, name='login'),
    path('signup/', views.SignUp, name='signup'),
    path('change_password/', views.change_password, name='change_password'),
    path('password_reset/', views.PasswordResetView.as_view(), name='password_reset'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.activate, name='activate'),

]
