from django.shortcuts import render
from django.views.generic import TemplateView

# Create your views here.
class homepage(TemplateView):
	template_name="base.html"



from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic
from .forms import CustomUserCreationForm, UserLoginForm, CustomUserPasswordResetForm, PasswordChangeForm, PasswordResetForm
from django.http import *
from django.shortcuts import render_to_response,redirect
from django.template import RequestContext
from .models import *
from django.contrib.auth.decorators import login_required
from django.contrib.auth import (
	authenticate,
	get_user_model,
	login,
	logout
)
from django.contrib import messages
from django.contrib.sites.shortcuts import get_current_site
from django.shortcuts import render, redirect
from django.template.loader import render_to_string

# For Authentication 
from .tokens import account_activation_token
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes
# from django.contrib.auth.forms import  PasswordResetForm

from django.contrib.auth import update_session_auth_hash






# Forgot password

from django.views.decorators.csrf import csrf_protect
from django.utils.decorators import method_decorator
from django.views.generic.edit import FormView
from django.contrib.auth.tokens import default_token_generator




from django.core.mail import EmailMessage

from django.core.mail import EmailMultiAlternatives

def SignUp(request):
    if request.method == 'POST':
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False

            user.save()

            to_email = form.cleaned_data.get('email')
            current_site = get_current_site(request)
            message = render_to_string('account_activation_email.html', {
                'user':user, 
                'domain':current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode(),
                'token': account_activation_token.make_token(CustomUser),
            })
            mail_subject = 'Activate your Easy Tailor account.'
            email = EmailMultiAlternatives(mail_subject, message, to=[to_email])
            email.attach_alternative(message, "text/html")
            email.send()
            return redirect('login')
    
    else:
        form = CustomUserCreationForm()
    
    return render(request, 'signup.html', {'form': form})


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = CustomUser.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, CustomUser.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(CustomUser, token):
        user.is_active = True
        user.email_confirmed = True
        user.save()
        return redirect('login')
    else:
        return render(request, 'invalid.html')



def login_view(request):
	next = request.GET.get('next')
	form = UserLoginForm(request.POST or None,)
	if form.is_valid():
		username = form.cleaned_data.get('email')
		password = form.cleaned_data.get('password')
		user = authenticate(username=username, password=password)
		login(request, user)
		messages.success(request, 'Your Logged in successfully!') 
		if next:
			return redirect(next)
		return redirect('/users/')
	context = { 'form':form, }
	return render(request, 'registration/login.html', context)


# @login_required
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            return redirect('/')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'registration/change_password.html', {
        'form': form
    })


class PasswordResetView(FormView):
    email_template_name = 'registration/password_reset_email.html'
    extra_email_context = None
    form_class = PasswordResetForm
    from_email = None
    html_email_template_name = None
    subject_template_name = 'registration/password_reset_subject.txt'
    success_url = reverse_lazy('password_reset_done')
    template_name = 'registration/password_reset_form.html'
    title = ('Password reset')
    token_generator = default_token_generator

    @method_decorator(csrf_protect)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def form_valid(self, form):
        opts = {
            'use_https': self.request.is_secure(),
            'token_generator': self.token_generator,
            'from_email': self.from_email,
            'email_template_name': self.email_template_name,
            'subject_template_name': self.subject_template_name,
            'request': self.request,
            'html_email_template_name': self.html_email_template_name,
            'extra_email_context': self.extra_email_context,
        }
        form.save(**opts)
        return super().form_valid(form)


INTERNAL_RESET_URL_TOKEN = 'set-password'
INTERNAL_RESET_SESSION_TOKEN = '_password_reset_token'

