from django.db import models
from django.contrib.auth.models import AbstractUser
from django_countries.fields import CountryField
from django.contrib.auth.models import User

# Create your models here.
from django.contrib.auth.models import PermissionsMixin, AbstractBaseUser, BaseUserManager

class CustomAccountManager(BaseUserManager):
    def create_user(self, email, password):
        user = self.model(email=email, password=password)
        user.set_password(password)
        user.is_staff = False
        user.is_superuser = False
        user.save()
        return user

    def create_superuser(self, email, password):
        user = self.create_user(email=email, password=password)
        user.is_active = True
        user.is_staff = True
        user.is_superuser = True
        user.save()
        return user

    def get_by_natural_key(self, email_):
        print(email_)
        return self.get(email=email_)


class CustomUser(AbstractUser):
    FullName                = models.CharField(max_length=100)
    
    Country                 = CountryField(blank_label='Select Country')
    email                   = models.EmailField(('email address'), unique=True)
    email_confirmed         = models.BooleanField(default=False)
    
    objects = CustomAccountManager()
    USERNAME_FIELD 			= 'email'
    REQUIRED_FIELDS 		= []
    def __str__(self):
        return self.email
    class Meta:
        verbose_name_plural = "Admin Registration"
CustomUser._meta.get_field('username')._unique = False
