from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, AuthenticationForm, PasswordResetForm
from .models import CustomUser
from django.contrib.auth import (
	authenticate,
	get_user_model,
	login,
	logout
)
from django.contrib.auth.forms import  PasswordChangeForm, PasswordResetForm


class CustomUserCreationForm(UserCreationForm):
	FullName = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'id': 'id_username','placeholder':"Full Name",}))
	# last_name = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}))
	email = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'id': 'id_username','placeholder':"Email",}))
	CompanyName = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'id': 'id_CompanyName','placeholder':"Company Name",}))
	password1 = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control','placeholder':"Password"}))
	password2 = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control', 'placeholder':"Confirm Password",}))
	class Meta(UserCreationForm):
		model = CustomUser
		fields = ('FullName', 'email', 'CompanyName','Country','password1','password2' )
		# fields = '__all__'
		exclude = ['username','first_name', 'last_name','password','groups', 'user_permissions', 'is_staff', 'is_active','is_superuser','last_login','date_joined', 'ConfirmPassword']



class CustomUserPasswordResetForm(PasswordResetForm):
	email = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'id': 'id_username','placeholder':"Email",}))
	class Meta(PasswordResetForm):
		model = CustomUser
		fields = ['email']

from django.contrib.auth.forms import PasswordResetForm

# class PasswordResetForm(PasswordResetForm):
# 	email = forms.CharField(label="",widget=forms.TextInput(attrs={'class':'form-control', 'id': 'id_username','placeholder':"Email",}))
# 	class Meta:
# 		model = CustomUser
# 		fields = '__all__'

# class PasswordResetForm(PasswordResetForm):
#  	email = forms.CharField(label="",widget=forms.TextInput(attrs={'class':'form-control', 'id': 'id_username','placeholder':"Email",}))
#  	class Meta:
#  		model = CustomUser
#  		fields = ("email")
#  	def clean_email(self):
#  		email = self.cleaned_data['email']
#  		if function_checkemaildomain(email) == False:
#  			raise forms.ValidationError("Untrusted email domain")
#  		elif function_checkemailstructure(email)==False:
#  			raise forms.ValidationError("This is not an email adress.")
#  		return email

 		
class UserLoginForm(forms.Form):
	email = forms.CharField(label="",widget=forms.TextInput(attrs={'class':'form-control', 'id': 'id_username','placeholder':"Email",}))
	password = forms.CharField(label="",widget=forms.PasswordInput(attrs={'class':'form-control','placeholder':"Password",}))

	def clean(self, *args, **kwargs):
		username = self.cleaned_data.get('email')
		password = self.cleaned_data.get('password')

		if username and password:
			user = authenticate(username=username, password=password)
			if not user:
				raise forms.ValidationError("Oops This user does not exist!!")
			if not user.check_password(password):
				raise forms.ValidationError("Inccorect password")
			if not user.is_active:
				raise forms.ValidationError("This user not active.")
		return super(UserLoginForm, self).clean(*args, **kwargs)


	class Meta:
		model = CustomUser
		fields = '__all__'

# class UserLoginForm(forms.Form):
# 	email = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'id': 'id_username','placeholder':"Email",}))
# 	password = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control', 'id': 'id_username','placeholder':"Password",}))

# 	def clean(self, *args, **kwargs):
# 		email = self.cleaned_data.get('email')
# 		password = self.cleaned_data.get('password')

# 		if email and password:
# 			AdminLogin = authenticate(username=email, password=password)
# 			if not AdminLogin:
# 				raise forms.ValidationError("This Admin Login does not exist!!.")
# 			if not AdminLogin.check_password(password):
# 				raise forms.ValidationError("Inccorect password")
# 			if not AdminLogin.is_active:
# 				raise forms.ValidationError("This Admin Login not active.")
# 		return super(UserLoginForm, self).clean(*args, **kwargs)
# 	class Meta:
# 		model = CustomUser
# 		fields = '__all__'
class PasswordChangeForm(PasswordChangeForm):
	old_password = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control','placeholder':"Old Password"}))
	new_password1 = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control', 'placeholder':"New Password",}))
	new_password2 = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control', 'placeholder':"New Password Confirm",}))



class PasswordResetForm(PasswordResetForm):
	email = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':"Email"}))

