from django.db import models
# from datetime import datetime

LEAVES =(("sick_leave","Sick Leave"),("casual_leave","Casual Leave"),("half_pay_leave","Half Pay Leave"),("earned_leave","Earned Leave"))


#Model for employees
class Employee(models.Model):
	name = models.CharField(max_length=100)
	email = models.EmailField(max_length=75)
	dob = models.CharField(max_length = 100 , default = '')
	mobile = models.BigIntegerField(default =True, unique=True)
	address = models.TextField(max_length=100, default = '',editable = False)
	gender=models.CharField(max_length=200,default = '',editable = False)

	def __str__(self):
		return self.name

class leave(models.Model):
	emp=models.ForeignKey(Employee,on_delete=models.CASCADE)
	leave_type=models.CharField(max_length=20,choices=LEAVES,)
	date_from = models.DateField(null=True)
	date_to	= models.DateField(null=True)
	days=models.IntegerField(default=0)
	notes=models.TextField(max_length=50)
	isApprove=models.BooleanField(default=False)
	def __str__(self):
		return self.emp+" - "+self.leave
